package com.jwt.code.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.code.dao.RoleRepository;
import com.jwt.code.model.Role;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RoleController {


	@Autowired
	private RoleRepository repo;

	@GetMapping("/roles")
	public List<Role> getAllRoles() {
		return (List<Role>) repo.findAll();
	}
}
