package com.jwt.code.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.jwt.code.dao.DepartmentRepo;
import com.jwt.code.dao.EmployeeDao;
import com.jwt.code.dao.RoleRepository;
import com.jwt.code.dto.EmployeDto;
import com.jwt.code.exeception.EmployeNotFoundException;
import com.jwt.code.model.Employee;



@Service
public class EmployeService {

	@Autowired
	private EmployeeDao employeRepo;
	@Autowired
	private RoleRepository roleDao;
	@Autowired
	private DepartmentRepo repo;
	
	public List<Employee> listAll(){	
	    return (List<Employee>) employeRepo.findAll();
	}
	
	public Employee save(EmployeDto employe) {
		Employee x = new Employee();
		
		x.setUsername(employe.getUsername());
		x.setEmail(employe.getEmail());
		if (employe.getRole() != null) {
			x.setRole(roleDao.findByRole(employe.getRole()).get());
			}else {
				x.setRole(roleDao.findByRole("USER").get());
			}
			
			x.setDepartment(repo.findByNomDep(employe.getDepartment()).get());
			
		
		BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(employe.getPassword());
		x.setPassword(encodedPassword);
		return employeRepo.save(x);
	}
	
	
	 
	 public Employee get(Long id) throws EmployeNotFoundException {
			try {
				return employeRepo.findById(id).get();
			} catch (NoSuchElementException ex) {
				throw new EmployeNotFoundException("Could not find any Employe with ID " + id);
			}
		}
	 
	 
		public void delete(Long id) throws EmployeNotFoundException {
			Long countById = employeRepo.countById(id);

			if (countById == null || countById == 0) {
				throw new EmployeNotFoundException("Could not find any Employe with ID " + id);			
			}

			employeRepo.deleteById(id);
		}
		
		 public Employee update(Long id,EmployeDto x) throws EmployeNotFoundException {
			
			 Long countById = employeRepo.countById(id);

				if (countById == null || countById == 0) {
					throw new EmployeNotFoundException("Could not find any Employe with ID " + id);			
				}

				Employee y =employeRepo.findById(id).get();
				y.setUsername(x.getUsername());
				y.setEmail(x.getEmail());
				BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
				String encodedPassword = passwordEncoder.encode(x.getPassword());
				y.setPassword(encodedPassword);
				if (x.getRole() != null) {
					y.setRole(roleDao.findByRole(x.getRole()).get());
					}else {
						y.setRole(roleDao.findByRole("USER").get());
					}
					
					y.setDepartment(repo.findByNomDep(x.getDepartment()).get());
				
				employeRepo.save(y);
			return y;
			}
		
	
}
