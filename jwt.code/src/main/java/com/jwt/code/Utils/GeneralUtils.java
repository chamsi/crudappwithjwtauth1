package com.jwt.code.Utils;

import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.jwt.code.model.Employee;

import com.jwt.code.model.Role;



public class GeneralUtils {
	
		 
	    public static List<SimpleGrantedAuthority> buildSimpleGrantedAuthorities(final Role role) {
	        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
	      
	            authorities.add(new SimpleGrantedAuthority(role.getRole()));
	     
	        return authorities;
	    }
	    
	   
}
