package com.jwt.code.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.code.dto.EmployeDto;
import com.jwt.code.exeception.EmployeNotFoundException;
import com.jwt.code.model.Employee;
import com.jwt.code.service.EmployeService;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeController {


	@Autowired
	private EmployeService service;

	@GetMapping("/emp")
	public List<Employee> getAllEmployees() {
		return service.listAll();
	}

	@GetMapping("/emp/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId)
			throws EmployeNotFoundException {
		Employee employee = service.get(employeeId);
				
		return ResponseEntity.ok().body(employee);
	}

	@PostMapping("/addEmp")
	public Employee createEmployee(@Valid @RequestBody EmployeDto user) {
		return service.save(user);
	}

	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody EmployeDto employeeDetails) throws EmployeNotFoundException {

		final Employee updatedEmployee = service.update(employeeId,employeeDetails);
		return ResponseEntity.ok(updatedEmployee);
	}

	@DeleteMapping("/employees/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
			throws EmployeNotFoundException {
		
		service.delete(employeeId);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
	
}
