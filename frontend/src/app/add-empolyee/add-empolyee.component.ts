import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Department, DepartmentService } from '../_services/department.service';
import { Role, RoleService } from '../_services/role.service';
import { Empolyee, UserService } from '../_services/user.service';
@Component({
  selector: 'app-add-empolyee',
  templateUrl: './add-empolyee.component.html',
  styleUrls: ['./add-empolyee.component.css']
})
export class AddEmpolyeeComponent implements OnInit {

  form: any = {
    username: null,
    email: null,
    password: null,
    role:null,
    department:null
  };
  isSuccessful = false;
 
  errorMessage = '';
  roles: Role[]= [];
  deps: Department[] = [];
  
 

  
   
  
    constructor(private depService: DepartmentService,private roleService: RoleService,private employeeService: UserService,
      private router: Router) { }

      ngOnInit(): void {
        this.depService.getDeps().subscribe(response=>this.handleSuccessfulResponse(response));
        this.roleService.getRoles().subscribe(response1=>this.handleSuccessfulResponse1(response1));
      }
      
      handleSuccessfulResponse(response: Department[]) {
        this.deps = response;
      }
      handleSuccessfulResponse1(response1: Role[]) {
        this.roles = response1;
      }

  

  
  onSubmit(): void {
    const { username, email, password,role,department } = this.form;

    this.employeeService.createEmployee(username, email, password,role,department).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.gotoList();
      },
      err => {
        this.errorMessage = err.error.message;
       
      }
    );
  }

  

  gotoList() {
    this.router.navigate(['/employees']);
  }
}
