import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export class Role {
  constructor(
    public id_Role: number,
    public role: string,
    
  ) {}
}
const API_URL = 'http://localhost:8080/';
@Injectable({
  providedIn: 'root'
})
export class RoleService {

 

  constructor(private http: HttpClient) { }

  getRoles() {
    return this.http.get<Role[]>(API_URL + 'roles');
  }

}
