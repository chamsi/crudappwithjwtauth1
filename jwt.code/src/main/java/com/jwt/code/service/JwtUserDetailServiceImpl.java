package com.jwt.code.service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jwt.code.Utils.GeneralUtils;
import com.jwt.code.model.Department;
import com.jwt.code.model.Employee;

public class JwtUserDetailServiceImpl implements UserDetails {
	  private static final long serialVersionUID = 1L;

	  private Long id;

	  private String username;

	  private String email;

	  @JsonIgnore
	  private String password;
	 
	  private Department department;
	  

	  private Collection<? extends SimpleGrantedAuthority> authorities;

	  public JwtUserDetailServiceImpl(Long id, String username, String email, String password,Department department,
	      Collection<? extends SimpleGrantedAuthority> authorities) {
	    this.id = id;
	    this.username = username;
	    this.email = email;
	    this.password = password;
	    this.department=department;
	    this.authorities = authorities;
	  }
	  

	  public static JwtUserDetailServiceImpl build(Employee user) {
	    List<SimpleGrantedAuthority> authorities = GeneralUtils.buildSimpleGrantedAuthorities(user.getRole());

	    return new JwtUserDetailServiceImpl(
	        user.getId(), 
	        user.getUsername(), 
	        user.getEmail(),
	        user.getPassword(), 
	        user.getDepartment(),
	        authorities);
	  }

	  @Override
	  public Collection<? extends SimpleGrantedAuthority> getAuthorities() {
	    return authorities;
	  }

	  public Long getId() {
	    return id;
	  }

	  public String getEmail() {
	    return email;
	  }

	  @Override
	  public String getPassword() {
	    return password;
	  }

	  @Override
	  public String getUsername() {
	    return username;
	  }

	  @Override
	  public boolean isAccountNonExpired() {
	    return true;
	  }

	  @Override
	  public boolean isAccountNonLocked() {
	    return true;
	  }

	  @Override
	  public boolean isCredentialsNonExpired() {
	    return true;
	  }

	  @Override
	  public boolean isEnabled() {
	    return true;
	  }

	  public Department getDepartment() {
		return department;
	}


	public void setDepartment(Department department) {
		this.department = department;
	}


	@Override
	  public boolean equals(Object o) {
	    if (this == o)
	      return true;
	    if (o == null || getClass() != o.getClass())
	      return false;
	    JwtUserDetailServiceImpl user = (JwtUserDetailServiceImpl) o;
	    return Objects.equals(id, user.id);
	  }}
