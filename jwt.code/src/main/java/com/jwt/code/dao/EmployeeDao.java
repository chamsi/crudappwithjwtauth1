package com.jwt.code.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


import com.jwt.code.model.Employee;
@Repository
public interface EmployeeDao extends JpaRepository<Employee, Long> {
	
	Optional<Employee> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	public Long countById(Long id);
}
