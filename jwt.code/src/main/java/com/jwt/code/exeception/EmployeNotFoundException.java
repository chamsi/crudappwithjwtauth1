package com.jwt.code.exeception;

public class EmployeNotFoundException extends Exception{
	
	
	public EmployeNotFoundException(String message) {
		super(message);
		
	}
}
