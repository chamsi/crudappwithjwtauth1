package com.jwt.code.model;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity

public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id_Role;


	@Column(length = 20)
	private String role;

	public Role() {

	}
	
	public Role(String role) {
    this.role=role;
	}

	public Integer getId_Role() {
		return Id_Role;
	}

	public void setId_Role(Integer id_Role) {
		Id_Role = id_Role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	
}
