package com.jwt.code.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "departments")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private Long Id_departement;
	
	@Column(nullable=false,unique=true)
	private String nomDep;

	
	
	public Department() {
		
	}
	
	public Department(String Nom_departement) {
		
		this.nomDep = Nom_departement;
	}

	@Override
	public String toString() {
		return "Departement [Id_departement=" + Id_departement + ", Nom_departement=" + nomDep + "]";
	}

	public Long getId_departement() {
		return Id_departement;
	}

	public void setId_departement(Long id_departement) {
		Id_departement = id_departement;
	}

	public String getNom_departement() {
		return nomDep;
	}

	public void setNom_departement(String nom_departement) {
		nomDep = nom_departement;
	}
	
	
}