import { Component, OnInit } from '@angular/core';
import { response } from 'express';
import { AuthService } from '../_services/auth.service';
import { Department, DepartmentService } from '../_services/department.service';
import { Role, RoleService } from '../_services/role.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {
    username: null,
    email: null,
    password: null,
    role:null,
    department:null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  roles: Role[]= [];
  deps: Department[] = [];

  constructor(private authService: AuthService,private depService: DepartmentService,private roleService: RoleService) { }

  ngOnInit(): void {
    this.depService.getDeps().subscribe(response=>this.handleSuccessfulResponse(response));
    this.roleService.getRoles().subscribe(response1=>this.handleSuccessfulResponse1(response1));
  }
  
  handleSuccessfulResponse(response: Department[]) {
    this.deps = response;
  }
  handleSuccessfulResponse1(response1: Role[]) {
    this.roles = response1;
  }

  onSubmit(): void {
    const { username, email, password,role,department } = this.form;

    this.authService.register(username, email, password,role,department).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
