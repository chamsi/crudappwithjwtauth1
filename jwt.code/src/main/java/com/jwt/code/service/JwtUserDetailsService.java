package com.jwt.code.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.jwt.code.dao.DepartmentRepo;
import com.jwt.code.dao.EmployeeDao;
import com.jwt.code.dao.RoleRepository;
import com.jwt.code.dto.EmployeDto;
import com.jwt.code.model.Employee;


@Service
public class JwtUserDetailsService implements UserDetailsService {
    
	@Autowired
	private EmployeeDao userDao;
	
	@Autowired
	private RoleRepository roleDao;
	@Autowired
	private DepartmentRepo depDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Override
	@Transactional
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
	
		Employee user = userDao.findByUsername(username).get();
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return JwtUserDetailServiceImpl.build(user) ;
	
	}

	public Employee save(EmployeDto user) {
		Employee newUser = new Employee();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setEmail(user.getEmail());
		if (user.getRole() != null) {
		newUser.setRole(roleDao.findByRole(user.getRole()).get());
		}else {
			newUser.setRole(roleDao.findByRole("USER").get());
		}
		
		newUser.setDepartment(depDao.findByNomDep(user.getDepartment()).get());
		return  userDao.save(newUser);
	}
	
	

}
