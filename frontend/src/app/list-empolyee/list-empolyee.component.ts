import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


import { TokenStorageService } from '../_services/token-storage.service';
import { Empolyee, UserService } from '../_services/user.service';

@Component({
  selector: 'app-list-empolyee',
  templateUrl: './list-empolyee.component.html',
  styleUrls: ['./list-empolyee.component.css']
})
export class ListEmpolyeeComponent implements OnInit {

  showButton = false;
  employees!: Observable<Empolyee[]>;
  isLoggedIn: boolean =false;
  roles: any;
  

  constructor(private empService: UserService,private tokenStorageService: TokenStorageService,private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
    
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
     this.showButton=this.roles.includes('SUPER_ADMIN')
  }
}
  reloadData() {
    this.employees = this.empService.getEmployeesList();
  }
  deleteEmployee(id: number) {
    this.empService.deleteEmployee(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
  updateEmployee(id: number){
    this.router.navigate(['update', id]);
  }
 


  
  
 

}
