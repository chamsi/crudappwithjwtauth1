import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Role } from './role.service';
import { Department } from './department.service';

const API_URL = 'http://localhost:8080/';

export class Empolyee {
  
    public id!: number;
    public username!: string;
    public email!: string;
    public password!: string;
    public role!: Role;
    public department!: Department;
    
   }
   const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {


   }
  
   getEmployee(id:number): Observable<any> {
    return this.http.get<any>("http://localhost:8080/emp"+"/"+id);
  }


  getEmployeesList(): Observable<any> {
    return this.http.get<Empolyee[]>("http://localhost:8080/emp");
  }


  public deleteEmployee(id:number) {
    return this.http.delete<string>(
      "http://localhost:8080/employees" + "/" + id
    );
  }

  public updateEmployee(id:number,username: string, email: string, password: string,role : string,department : string): Observable<any> {
    return this.http.put(
      "http://localhost:8080/employees"+"/"+id,{
        username,
        email,
        password,
        role,
        department,
      }, httpOptions);
      }
      
    
  

  
  createEmployee(username: string, email: string, password: string,role : string,department : string): Observable<any> {
    return this.http.post("http://localhost:8080/addEmp", {
      username,
      email,
      password,
      role,
      department,
    }, httpOptions);
  }
}


