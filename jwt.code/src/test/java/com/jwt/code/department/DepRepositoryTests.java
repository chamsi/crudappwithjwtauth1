package com.jwt.code.department;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.jwt.code.dao.DepartmentRepo;
import com.jwt.code.dao.RoleRepository;
import com.jwt.code.model.Department;
import com.jwt.code.model.Role;





@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@Rollback(false)
public class DepRepositoryTests {
@Autowired
private DepartmentRepo repo;
@Test
public void testCreateFirstRole() {
	
	Department Dep = new Department("IT");
	Department savedDep = repo.save(Dep);
	
	/*The assertThat is one of the JUnit methods from the Assert object that can be used to check if a specific value match to an expected one.*/
	assertThat(savedDep.getId_departement()).isGreaterThan(0);
	
	
	Department Dep1 = new Department("Data");
	Department savedDep1 = repo.save(Dep1);
	
	/*The assertThat is one of the JUnit methods from the Assert object that can be used to check if a specific value match to an expected one.*/
	assertThat(savedDep1.getId_departement()).isGreaterThan(0);
}
}


