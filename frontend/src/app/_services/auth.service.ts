import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

const AUTH_API = 'http://localhost:8080/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  
  constructor(private http: HttpClient, public router: Router,public jwtHelper: JwtHelperService) { }


  public isAuthenticated(): boolean {
    const token = window.sessionStorage.getItem('auth-token');
    
    return !this.jwtHelper.isTokenExpired(token!);
  }

 

  login(username: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'authenticate', {
      username,
      password
    }, httpOptions);
  }

  register(username: string, email: string, password: string,role : string,department : string): Observable<any> {
    return this.http.post(AUTH_API + 'register', {
      username,
      email,
      password,
      role,
      department,
    }, httpOptions);
  }

  
}
