package com.jwt.code.role;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.jwt.code.dao.RoleRepository;
import com.jwt.code.model.Role;





@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@Rollback(false)
public class RoleRepositoryTests {
@Autowired
private RoleRepository repo;
@Test
public void testCreateFirstRole() {
	
	Role customer = new Role("USER");
	Role savedRole = repo.save(customer);
	
	/*The assertThat is one of the JUnit methods from the Assert object that can be used to check if a specific value match to an expected one.*/
	assertThat(savedRole.getId_Role()).isGreaterThan(0);
	
	
	Role customer1 = new Role("SUPER_ADMIN");
	Role savedRole1 = repo.save(customer1);
	
	/*The assertThat is one of the JUnit methods from the Assert object that can be used to check if a specific value match to an expected one.*/
	assertThat(savedRole1.getId_Role()).isGreaterThan(0);
}
}


