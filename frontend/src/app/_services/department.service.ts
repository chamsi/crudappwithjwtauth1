import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export class Department {
  constructor(
    public id_departement: number,
    public nom_departement: string,
    
  ) {}
}
const API_URL = 'http://localhost:8080/';
@Injectable({
  providedIn: 'root'
})



export class DepartmentService {

  constructor(private http: HttpClient) { }

  getDeps() {
    return this.http.get<Department[]>("http://localhost:8080/deps");
  }
  
}
