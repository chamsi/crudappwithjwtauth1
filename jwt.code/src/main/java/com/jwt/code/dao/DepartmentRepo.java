package com.jwt.code.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jwt.code.model.Department;


@Repository
public interface DepartmentRepo  extends CrudRepository<Department, Long> {

	Optional<Department> findByNomDep(String name);
}
