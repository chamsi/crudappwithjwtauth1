import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';



import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ListEmpolyeeComponent } from './list-empolyee/list-empolyee.component';
import { UpdateEmpolyeeComponent } from './update-empolyee/update-empolyee.component';
import { AddEmpolyeeComponent } from './add-empolyee/add-empolyee.component';

import { JwtHelperService, JWT_OPTIONS  } from '@auth0/angular-jwt';
import { AuthGardService } from './_services/auth-gard.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,   
    ProfileComponent,
    ListEmpolyeeComponent,
    UpdateEmpolyeeComponent,
    AddEmpolyeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [authInterceptorProviders,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,AuthGardService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
