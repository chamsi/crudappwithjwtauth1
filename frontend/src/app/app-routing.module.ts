import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { ProfileComponent } from './profile/profile.component';

import { ListEmpolyeeComponent } from './list-empolyee/list-empolyee.component';
import { AddEmpolyeeComponent } from './add-empolyee/add-empolyee.component';
import { UpdateEmpolyeeComponent } from './update-empolyee/update-empolyee.component';
import { 
  AuthGardService  as AuthGuard 
} from './_services/auth-gard.service';


const routes: Routes = [
  
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent,canActivate: [AuthGuard]  },
  { path: 'employees', component: ListEmpolyeeComponent,canActivate: [AuthGuard] },
  { path: 'addemploye', component: AddEmpolyeeComponent,canActivate: [AuthGuard] },
  { path: 'update/:id', component: UpdateEmpolyeeComponent,canActivate: [AuthGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
