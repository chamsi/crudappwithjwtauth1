import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Department, DepartmentService } from '../_services/department.service';
import { Role, RoleService } from '../_services/role.service';
import { Empolyee, UserService } from '../_services/user.service';

@Component({
  selector: 'app-update-empolyee',
  templateUrl: './update-empolyee.component.html',
  styleUrls: ['./update-empolyee.component.css']
})
export class UpdateEmpolyeeComponent implements OnInit {


isSuccessful = false;
form!: Empolyee;
id!: number;
errorMessage = '';
roles: Role[]= [];
deps: Department[] = [];




 

  constructor(private depService: DepartmentService,private roleService: RoleService,private employeeService: UserService,
    private router: Router,private route: ActivatedRoute) { }

    ngOnInit(): void {
      this.depService.getDeps().subscribe(response=>this.handleSuccessfulResponse(response));
      this.roleService.getRoles().subscribe(response1=>this.handleSuccessfulResponse1(response1));
      this.form = new Empolyee()
      this.id = Number(this.route.snapshot.paramMap.get('id'));
      
      this.employeeService.getEmployee(this.id)
      .subscribe(data => {
        console.log(data)
        this.form = data;
      }, error => console.log(error));
    }
    
    handleSuccessfulResponse(response: Department[]) {
      this.deps = response;
    }
    handleSuccessfulResponse1(response1: Role[]) {
      this.roles = response1;
    }


   

onSubmit(): void {
  const { id,username, email, password,role,department } = this.form;

  this.employeeService.updateEmployee(id,username, email, password,role.role,department.nom_departement).subscribe(
    data => {
      console.log(data);
      this.isSuccessful = true;
      this.gotoList();
    },
    err => {
      this.errorMessage = err.error.message;
     
    }
  );
}



gotoList() {
  this.router.navigate(['/employees']);
}}