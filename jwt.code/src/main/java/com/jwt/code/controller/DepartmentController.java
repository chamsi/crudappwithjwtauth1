package com.jwt.code.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jwt.code.model.Department;
import com.jwt.code.dao.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DepartmentController {

	@Autowired
	private DepartmentRepo repo;

	@GetMapping("/deps")
	public List<Department> getAllDepartments() {
		return (List<Department>) repo.findAll();
	}
}
